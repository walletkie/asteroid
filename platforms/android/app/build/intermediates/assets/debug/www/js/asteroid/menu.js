export default {
    MENU: null,
    FIREBASE: null,

    menuBox: {
        scores: null,
        score: null,
        main: null
    },

    currentStage: null,
    STGES: {
        NONE: 0,
        MAIN: 1,
        SCORES: 2
    },

    lastScore: null,

    init(menu, firebase) {
        this.MENU = menu;
        this.FIREBASE = firebase;

        this.menuBox.score = this.MENU.querySelector("#score-box");
        this.menuBox.scores = this.menu.querySelector("#scores-box");
        this.menuBox.main = this.MENU.querySelector("#score-box");
    },

    setMenuStage(stage) {
        if (stage == this.STGES.NONE) {
            console.log(this.STGES.NONE);
        } else if (stage == this.STGES.MAIN) {
            console.log(this.STGES.MAIN);
        } else if (stage == this.STGES.SCORES) {
            console.log(this.STGES.SCORES);
        } else {
            throw "Unknow menu stage";
        }
    }
};