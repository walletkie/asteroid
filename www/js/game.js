import Asteroid from "./asteroid/asteroid.js";
import Firebase from "./asteroid/firebase.js";

var app = {
    initialize() {
        document.addEventListener("deviceready", this.onDeviceReady.bind(this), false);
    },

    onDeviceReady() {
        window.screen.orientation.lock("portrait-primary");

        Firebase.initFirebase();

        var appElement = document.getElementById("app");
        var canvasElement = document.getElementById("canvas");
        var menuElement = document.getElementById("menu");

        Asteroid.init(appElement, canvasElement, menuElement, Firebase);
    }
};

app.initialize();