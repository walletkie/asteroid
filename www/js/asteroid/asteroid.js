export default {
    app: null,
    canvas: null,
    ctx: null,
    menu: null,
    scoreBox: null,
    Firebase: null,

    startButton: null,
    scoresButton: null,
    scoresList: null,
    showingScores: false,

    tick: 0,

    score: 0,
    scoreFrequent: 200,

    playerLife: 100,
    shipRadius: 4,
    planetRadius: 13,

    screen: {
        x: 100,
        y: 150,
        center: {
            x: 0,
            y: 0
        },
        shipRadius: 22
    },

    atlas: {
        bg: "./img/bg.png",
        ship: "./img/ship.png",
        shot: "./img/shot.png",
        asteroidSmall: "./img/asteroid_small.png",
        asteroidBig: "./img/asteroid_big.png",
        planet: "./img/planet.png",
        life: "./img/life.png"
    },

    motionSpeed: 0.1,

    shots: {},
    shotsLimit: 20,
    shotsFrequent: 5,
    shotsSpeed: 3,

    asteroids: {},
    asteroidsDistanceLimit: 400,
    asteroidsFrequent: 20,
    asteroidsSpeedMin: 0.4,
    asteroidsSpeedMax: 1.1,
    rotationSpeedMin: -0.4,
    rotationSpeedMax: 0.4,

    asteroidsGenerate: {
        topXMin: -50.0,
        topXMax: 250,
        topYMin: -20,
        topYMax: -50,

        bottomXMin: -50.0,
        bottomXMax: 250,
        bottomYMin: 370,
        bottomYMax: 400,
    },

    setMenuVisibility(visible) {
        this.menu.style.display = visible ? "block" : "none";
    },

    _paused: false,
    setGamePaused(paused) {
        this._paused = paused ? true : false;
    },

    resetGameStatus() {
        this.tick = 0;
        this.score = 0;
        this.shots = {};
        this.asteroids = {};
        this.playerLife = 100;
    },

    _uid: 0,
    getUID() {
        this._uid++;
        return this._uid;
    },

    updateScores() {
        this.scoresList.innerHTML = "Loading...";

        this.Firebase.getScores().then(result => {
            let fireScores = [];
            result.docs.forEach(e => {
                fireScores.push(Number(e.data().value));
            });
            fireScores = fireScores.sort((a, b) => {
                return b - a;
            });
            console.log(fireScores);
            this.scoresList.innerHTML = "";

            let limit = (5 > fireScores.length) ? fireScores.length : 5;
            for (let i = 0; i < limit; i++) {
                const element = fireScores[i];
                let s = document.createElement("span");
                s.innerHTML = element;
                this.scoresList.appendChild(s);
            }
        }).catch(result => {
            console.error(result);
        });
    },

    init(appElement, canvasElement, menuElement, Firebase) {
        this.Firebase = Firebase;
        this.app = appElement;
        this.canvas = canvasElement;
        this.ctx = canvasElement.getContext("2d");
        this.menu = menuElement;
        this.scoreBox = this.menu.querySelector("#score-box");
        this.scoresBox = this.menu.querySelector("#scores-box");
        this.scoresList = this.scoresBox.querySelector("#scores-list");

        this.startButton = this.menu.querySelector("#start");
        this.startButton.addEventListener("click", () => {
            this.resetGameStatus();
            this.setGamePaused(false);
            this.setMenuVisibility(false);
        });

        this.scoresButton = this.menu.querySelector("#scores");
        this.scoresButton.addEventListener("click", () => {
            this.setScoresBoxVisible(!this.showingScores);
            this.updateScores();
        });

        this.screen.center.x = (this.screen.x / 2.0);
        this.screen.center.y = (this.screen.y / 2.0);

        this.canvas.setAttribute("width", this.screen.x);
        this.canvas.setAttribute("heigth", this.screen.y);
        this.setpixelated(this.ctx);

        this.loadAtlas(() => this.start());

        this.initAcceleration();
        navigator.accelerometer.watchAcceleration(
            (e) => this.handleAcceleration(e),
            (err) => console.log(err), { frequency: 40 });

        this.ctx.font = "9px Consolas";
        this.ctx.fillStyle = "#ceb602";
        this.resetGameStatus();
        this.setGamePaused(true);
        this.setMenuVisibility(true);
        this.setScoreBoxVisible(false);
        this.setScoresBoxVisible(false);
    },

    setScoreBoxVisible(visible) {
        this.scoreBox.style.display = visible ? "block" : "none";
    },

    setScoresBoxVisible(visible) {
        this.showingScores = visible;
        this.startButton.style.display = visible ? "none" : "block";
        this.scoresBox.style.display = visible ? "block" : "none";
        this.scoreBox.style.display = (!visible && this.score !== 0) ? "block" : "none";
        this.scoresButton.innerHTML = visible ? "back" : "scores";
    },

    start() {
        let timer = () => {
            this.update();
            setTimeout(timer, 40);
        };
        timer();
    },

    setpixelated(context) {
        context["imageSmoothingEnabled"] = false; /* standard */
        context["mozImageSmoothingEnabled"] = false; /* Firefox */
        context["oImageSmoothingEnabled"] = false; /* Opera */
        context["webkitImageSmoothingEnabled"] = false; /* Safari */
        context["msImageSmoothingEnabled"] = false; /* IE */
    },

    loadAtlas(callback) {
        this._atlasLoadCounter = 0;
        this._atlasToLoad = Object.keys(this.atlas).length;

        for (let key in this.atlas) {
            let image = new Image();
            image.src = this.atlas[key];
            image.onload = () => {
                this._atlasLoadCounter++;
                if (this._atlasLoadCounter === this._atlasToLoad) {
                    callback();
                }
            };
            this.atlas[key] = image;
        }
    },

    initAcceleration() {
        this._current = {
            x: 0.0,
            y: 1.0,
            angle: 0.0
        };
        this._currentPos = {
            x: 0,
            y: 0
        };
    },

    toUnit(v) {
        let l = Math.sqrt((v.x * v.x) + (v.y * v.y));
        v.x /= l;
        v.y /= l;
        return v;
    },

    clearCanvas() {
        this.ctx.clearRect(0, 0, this.screen.x, this.screen.y);
    },

    drawBg() {
        this.ctx.drawImage(this.atlas.bg, 0, 0);
    },

    gameEnded() {
        this.Firebase.saveScore(this.score);
        this.scoreBox.innerHTML = "Score: " + this.score;
        this.setScoreBoxVisible(true);
    },

    update() {
        if (this._paused) return;

        this.tick++;

        if (this.playerLife < 0) {
            this.setGamePaused(true);
            this.setMenuVisibility(true);
            this.gameEnded();

            this.clearCanvas();
            this.drawBg();
            return;
        }

        this.clearCanvas();
        this.drawBg();

        this.updateShip();
        this.updatePlanet();
        this.updateLife();
        this.updateShots();
        this.updateScore();
        this.updateAsteroids();

        this.checkCollisions();
    },

    handleAcceleration(e) {
        let pos = {
            x: -e.x,
            y: e.y
        };

        if (pos.x > 10.0) pos.x = 10.0;
        if (pos.x < -10.0) pos.x = -10.0;

        if (pos.y > 10.0) pos.y = 10.0;
        if (pos.y < -10.0) pos.y = -10.0;

        if (pos.x > 1.5 || pos.x < -1.5) {
            let dx = pos.x - this._current.x;
            dx *= this.motionSpeed;
            this._current.x += dx;
        }

        if (pos.y > 1.5 || pos.y < -1.5) {
            let dy = pos.y - this._current.y;
            dy *= this.motionSpeed;
            this._current.y += dy;
        }

        this.toUnit(this._current);
        this._current.angle = Math.atan2(this._current.y, this._current.x) + 1.57079633;
    },

    updateShip() {
        this._currentPos.x = (this._current.x * this.screen.shipRadius) + this.screen.center.x;
        this._currentPos.y = (this._current.y * this.screen.shipRadius) + this.screen.center.y;

        this.ctx.save();
        this.ctx.translate(this._currentPos.x, this._currentPos.y);
        this.ctx.rotate(this._current.angle);
        this.ctx.drawImage(this.atlas.ship, -5, -5);
        this.ctx.restore();
    },

    updatePlanet() {
        this.ctx.drawImage(this.atlas.planet, this.screen.center.x - this.planetRadius, this.screen.center.y - this.planetRadius);
    },

    updateLife() {
        let sprite;
        if (this.playerLife < 10) {
            sprite = 0;
        } else if (this.playerLife < 30) {
            sprite = 1;
        } else if (this.playerLife < 50) {
            sprite = 2;
        } else if (this.playerLife < 75) {
            sprite = 3;
        } else {
            sprite = 4;
        }
        sprite *= 20;

        this.ctx.drawImage(this.atlas.life,
            sprite,
            0,
            20,
            20,
            this.screen.center.x - 9,
            this.screen.center.y - 8,
            20,
            20);
    },

    createShotObject(x, y, dist, angle) {
        return {
            dist: dist,
            x: x,
            y: y,
            angle: angle,
            _current: {
                x: 0,
                y: 0
            }
        };
    },

    updateShots() {
        for (let i in this.shots) {
            let shot = this.shots[i];
            shot.dist += this.shotsSpeed;
        }

        if (this.tick % this.shotsFrequent === 0) {
            // push new on the back
            let newShot = this.createShotObject(this._current.x, this._current.y, this.screen.shipRadius + 5, this._current.angle);
            this.shots[this.getUID()] = newShot;

            // delete first
            if (Object.keys(this.shots).length > this.shotsLimit) {
                for (let j in this.shots) {
                    delete this.shots[j];
                    break;
                }
            }
        }

        for (let i in this.shots) {
            let shot = this.shots[i];
            shot._current.x = (shot.x * shot.dist) + this.screen.center.x;
            shot._current.y = (shot.y * shot.dist) + this.screen.center.y;

            this.ctx.save();
            this.ctx.translate(shot._current.x, shot._current.y);
            this.ctx.rotate(shot.angle);
            this.ctx.drawImage(this.atlas.shot, -1, -1);
            this.ctx.restore();
        }
    },

    random(min, max) {
        return (Math.random() * (max - min)) + min;
    },

    createAsteroidObject(x, y, directionX, directionY, moveSpeed, rotationSpeed, texture, radius, life) {
        return {
            texture: texture,
            radius: radius,
            x: x,
            y: y,
            directionX: directionX,
            directionY: directionY,
            dist: 0.0,
            rotate: 0.0,
            moveSpeed: moveSpeed,
            rotationSpeed: rotationSpeed,
            life: life,
            _current: {
                x: 0,
                y: 0
            }
        };
    },

    checkCollisionBetweenAsteroids(asteroid1, asteroid2) {
        let dist = this.distTo(asteroid1._current, asteroid2._current);
        return (dist < asteroid1.radius + asteroid2.radius);
    },

    updateAsteroids() {
        for (let i in this.asteroids) {
            let asteroid = this.asteroids[i];
            asteroid.dist += asteroid.moveSpeed;
            asteroid.rotate += asteroid.rotationSpeed;
        }

        if (this.tick % this.asteroidsFrequent === 0) {
            let isBig = Math.random() < 0.5;
            let isFromUp = Math.random() < 0.5;
            let x, y, destX, destY, texture, radius, life;
            if (isBig) {
                texture = this.atlas.asteroidBig;
                radius = 11;
                life = 3;
            } else {
                texture = this.atlas.asteroidSmall;
                radius = 7;
                life = 1;
            }
            if (isFromUp) {
                x = this.random(this.asteroidsGenerate.bottomXMin, this.asteroidsGenerate.bottomXMax);
                y = this.random(this.asteroidsGenerate.bottomYMin, this.asteroidsGenerate.bottomYMax);
                destX = this.random(this.asteroidsGenerate.topXMin, this.asteroidsGenerate.topXMax);
                destY = this.random(this.asteroidsGenerate.topYMin, this.asteroidsGenerate.topYMax);
            } else {
                x = this.random(this.asteroidsGenerate.topXMin, this.asteroidsGenerate.topXMax);
                y = this.random(this.asteroidsGenerate.topYMin, this.asteroidsGenerate.topYMax);
                destX = this.random(this.asteroidsGenerate.bottomXMin, this.asteroidsGenerate.bottomXMax);
                destY = this.random(this.asteroidsGenerate.bottomYMin, this.asteroidsGenerate.bottomYMax);
            }
            let direction = {
                x: destX - x,
                y: destY - y
            };
            this.toUnit(direction);
            let moveSpeed = this.random(this.asteroidsSpeedMin, this.asteroidsSpeedMax);
            let rotationSpeed = this.random(this.rotationSpeedMin, this.rotationSpeedMax);
            let newAsteroid = this.createAsteroidObject(x, y, direction.x, direction.y, moveSpeed, rotationSpeed, texture, radius, life);
            this.asteroids[this.getUID()] = newAsteroid;
        }

        for (let i in this.asteroids) {
            let asteroid = this.asteroids[i];
            if (asteroid.dist > this.asteroidsDistanceLimit) {
                delete this.asteroids[i];
            }
        }

        // reset before direction change
        // for (let i in this.asteroids) {
        //     let asteroid = this.asteroids[i];
        //     asteroid.directionChanged = false;
        // }

        for (let i in this.asteroids) {
            let asteroid = this.asteroids[i];
            asteroid._current.x = asteroid.x + (asteroid.directionX * asteroid.dist);
            asteroid._current.y = asteroid.y + (asteroid.directionY * asteroid.dist);

            // for (let j in this.asteroids) {
            //     j = this.asteroids[j];
            //     if (asteroid != j && asteroid.directionChanged == false && j.directionChanged == false) {
            //         if (this.checkCollisionBetweenAsteroids(asteroid, j)) {
            //             asteroid.directionChanged = true;
            //             j.directionChanged = true;

            //             let tempX = asteroid.directionX;
            //             let tempY = asteroid.directionY;
            //             asteroid.directionX = j.directionX;
            //             asteroid.directionY = j.directionY;
            //             j.directionX = tempX;
            //             j.directionY = tempY;

            //             asteroid._current.x = asteroid.x + (asteroid.directionX * asteroid.dist);
            //             asteroid._current.y = asteroid.y + (asteroid.directionY * asteroid.dist);
            //             j._current.x = asteroid.x + (asteroid.directionX * asteroid.dist);
            //             j._current.y = asteroid.y + (asteroid.directionY * asteroid.dist);
            //         }
            //     }
            // }

            this.ctx.save();
            this.ctx.translate(asteroid._current.x, asteroid._current.y);
            this.ctx.rotate(asteroid.rotate);
            this.ctx.drawImage(asteroid.texture, -asteroid.radius, -asteroid.radius);
            this.ctx.restore();
        }
    },

    checkCollisions() {
        for (let i in this.asteroids) {
            let asteroid = this.asteroids[i];
            for (let j in this.shots) {
                let shot = this.shots[j];
                if (this.checkAsteroidCollisonWithShot(asteroid, shot)) {
                    delete this.shots[j];
                    asteroid.life--;
                    if (asteroid.life <= 0) {
                        delete this.asteroids[i];
                    }
                }
            }
        }

        for (let i in this.asteroids) {
            let asteroid = this.asteroids[i];

            if (this.distTo(asteroid._current, this.screen.center) < (asteroid.radius + this.planetRadius)) {
                this.playerLife -= asteroid.life * 10;
                navigator.vibrate(500);
                delete this.asteroids[i];
            }

            if (this.distTo(asteroid._current, this._currentPos) < (asteroid.radius + this.shipRadius)) {
                this.playerLife = -1;
                navigator.vibrate(500);
                delete this.asteroids[i];
            }
        }
    },

    distTo(v1, v2) {
        return Math.sqrt(Math.pow(v2.x - v1.x, 2) + Math.pow(v2.y - v1.y, 2));
    },

    checkAsteroidCollisonWithShot(asteroid, shot) {
        return this.distTo(asteroid._current, shot._current) < asteroid.radius;
    },

    updateScore() {
        if (this.tick % this.scoreFrequent) {
            this.score += 1;
        }

        this.ctx.fillText(this.score, 6, 142);
    }
};