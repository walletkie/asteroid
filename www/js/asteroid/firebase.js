/* eslint-disable-undef */

export default {
    initFirebase() {
        // Initialize Firebase
        let config = {
            apiKey: "AIzaSyDG9RKKX2GOo-bmPsF1g_j8QP-0qUAoaC4",
            authDomain: "asteroid-cee23.firebaseapp.com",
            databaseURL: "https://asteroid-cee23.firebaseio.com",
            projectId: "asteroid-cee23",
            storageBucket: "asteroid-cee23.appspot.com",
            messagingSenderId: "616394380195"
        };
        firebase.initializeApp(config);
    },

    initUI(elementId) {
        // FirebaseUI config.
        var uiConfig = {
            signInSuccessUrl: "game.html",
            signInOptions: [
                firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                firebase.auth.FacebookAuthProvider.PROVIDER_ID,
                firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
            ],
            // tosUrl and privacyPolicyUrl accept either url string or a callback function.
            // Terms of service url/callback.
            tosUrl: "login.html",
            // Privacy policy url/callback.
            privacyPolicyUrl() {
                window.location.assign("login.html");
            }
        };

        // Initialize the FirebaseUI Widget using Firebase.
        // eslint-disable-next-line
        var ui = new firebaseui.auth.AuthUI(firebase.auth());
        // The start method will wait until the DOM is loaded.
        ui.start(elementId, uiConfig);
    },

    getScores: async function() {
        let firebaseDB;
        let firebaseUser;
        await firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                firebaseUser = user;
                firebaseDB = firebase.firestore();
                firebaseDB.settings({ timestampsInSnapshots: true });
            }
        });

        return firebaseDB.collection("users").doc(firebaseUser.uid).collection("scores").get();
    },

    saveScore(score) {
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                let db = firebase.firestore();
                db.settings({ timestampsInSnapshots: true });

                db.collection("users").doc(user.uid).collection("scores").add({
                        value: 123
                    })
                    .then(function(docRef) {
                        console.log("Document written" + docRef.id);
                    })
                    .catch(function(error) {
                        console.error("Error adding document: ", error);
                    });

            }
        });
    },

    test() {
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                console.log(user);

                let db = firebase.firestore();
                db.settings({ timestampsInSnapshots: true });

                db.collection("users").doc(user.uid).set({
                        name: "Pioter!"
                    })
                    .then(function() {
                        console.log("Document written");
                    })
                    .catch(function(error) {
                        console.error("Error adding document: ", error);
                    });

                db.collection("users").doc(user.uid).collection("scores").add({
                        value: 123
                    })
                    .then(function(docRef) {
                        console.log("Document written" + docRef.id);
                    })
                    .catch(function(error) {
                        console.error("Error adding document: ", error);
                    });

            } else {
                // No user is signed in.
            }
        });
    }
};