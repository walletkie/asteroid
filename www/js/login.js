import Firebase from "./asteroid/firebase.js";

var app = {
    initialize() {
        document.addEventListener("deviceready", this.onDeviceReady.bind(this), false);
    },

    onDeviceReady() {
        window.screen.orientation.lock("portrait-primary");


        Firebase.initFirebase();
        Firebase.initUI("#firebase");
    }
};

app.initialize();