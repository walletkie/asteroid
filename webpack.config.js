module.exports = {
    mode: "production",
    entry: {
        game: "./www/js/game.js",
        login: "./www/js/login.js"
    },
    output: {
        filename: "webpack_[name].js",
        path: __dirname + "/www/js"
    }
};